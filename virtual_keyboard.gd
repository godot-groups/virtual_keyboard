extends Control
tool

export(Resource) var layout = null

const virtual_keyboard_layout_const = preload("virtual_keyboard_layout.gd")

func button_string_callback(p_string):
	print(p_string)
	
static func create_button(p_owner, p_button_info, p_shift):
	var button = Button.new()
	
	if p_button_info.get_expand():
		button.set_h_size_flags(SIZE_EXPAND_FILL)
	else:
		button.set_h_size_flags(SIZE_FILL)
	
	if p_button_info.get_button_type() == virtual_keyboard_layout_const.VKButton.VK_BUTTON_UNICODE:
		if p_shift:
			button.connect("pressed", p_owner, "button_string_callback", [p_button_info.get_shift_input_string()])
		else:
			button.connect("pressed", p_owner, "button_string_callback", [p_button_info.get_input_string()])
			
	if p_shift:
		button.set_text(p_button_info.get_shift_input_string())
	else:
		button.set_text(p_button_info.get_input_string())
		
	return button

func create_from_layout(p_layout, p_shift):
	if p_layout is virtual_keyboard_layout_const:
		for i in range(0, p_layout.row_count):
			var row = p_layout.rows[i]
			var horizontal_layout = HBoxContainer.new()
			for j in range(0, row.button_count):
				var button_info = row.buttons[j]
				var button = create_button(self, button_info, p_shift)
				
				horizontal_layout.add_child(button)
			add_child(horizontal_layout)
	else:
		printerr("Invalid layout format!")

func _ready():
	if layout:
		create_from_layout(layout, false)